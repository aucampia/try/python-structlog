import inspect
import json
import logging
import multiprocessing
import os
import sys
import threading
from io import StringIO
from typing import Any, Callable, Dict, Optional, Set

import pytest
from structlog.processors import JSONRenderer
from structlog.stdlib import BoundLogger, ProcessorFormatter

from aucampia.trx.structlog.extras import (
    CallsiteInfoAdder,
    CallsiteParameter,
    add_extra,
    callsite_parameters,
    get_processname,
)


@pytest.fixture
def log_record() -> Callable[..., logging.LogRecord]:
    """
    A LogRecord factory.
    """

    def create_log_record(**kwargs: Any) -> logging.LogRecord:
        defaults = {
            "name": "sample-name",
            "level": logging.INFO,
            "pathname": None,
            "lineno": None,
            "msg": "sample-message",
            "args": [],
            "exc_info": None,
        }
        defaults.update(kwargs)
        return logging.LogRecord(**defaults)  # type: ignore[arg-type]

    return create_log_record


class TestAddExtra:
    def test_add_extra(self, log_record: Callable[[], Any]) -> None:
        """
        Extra attributes of a LogRecord are added to the event dict.
        """
        extra = {"x_this": "is", "x_the": 3, "x_extra": "values"}
        record: logging.LogRecord = log_record()
        record.__dict__.update(extra)
        event_dict = {"_record": record}
        event_dict_out = add_extra(None, None, event_dict)  # type: ignore[arg-type]
        assert event_dict_out == {**event_dict, **extra}  # type: ignore[arg-type]

    def test_add_extra_e2e(self) -> None:
        """
        Values passed in the extra parameter of log methods are added as
        members of JSON output objects.
        """
        logger = logging.Logger(sys._getframe().f_code.co_name)
        string_io = StringIO()
        handler = logging.StreamHandler(string_io)
        formatter = ProcessorFormatter(
            foreign_pre_chain=[add_extra],
            processors=[JSONRenderer()],
        )
        handler.setFormatter(formatter)
        handler.setLevel(0)
        logger.addHandler(handler)
        logger.setLevel(0)
        extra = {"with": "values", "from": "extra"}
        logger.info("Some %s", "text", extra=extra)
        out_item_set = set(json.loads(string_io.getvalue()).items())
        expected_item_set = set({"event": "Some text", **extra}.items())
        assert expected_item_set.issubset(out_item_set), (
            f"expected_item_set={expected_item_set} should"
            f"be a subset of out_item_set={out_item_set}"
        )


class TestProcessname:
    def test_default(self) -> None:
        assert get_processname() == multiprocessing.current_process().name

    def test_changed(self, monkeypatch: pytest.MonkeyPatch) -> None:
        tmp_name = "fakename"
        monkeypatch.setattr(
            target=multiprocessing.current_process(),
            name="name",
            value=tmp_name,
        )
        assert get_processname() == tmp_name


class TestCallsiteInfoAdder:
    parameter_strings = {
        "pathname",
        "filename",
        "module",
        "funcName",
        "lineno",
        "thread",
        "threadName",
        "process",
        "processName",
    }

    @pytest.mark.parametrize(
        "parameter_strings",
        [
            None,
            *[{parameter} for parameter in parameter_strings],
            set(),
            parameter_strings,
            {"pathname", "filename"},
            {"module", "funcName"},
        ],
    )
    def test_logging_log(
        self,
        parameter_strings: Optional[Set[str]],
    ) -> None:
        processors = []
        logging.info("parameter_strings = %s", parameter_strings)
        if parameter_strings is None:
            processors.append(CallsiteInfoAdder())
            parameter_strings = self.parameter_strings
        else:
            parameters = self.filter_parameters(parameter_strings)
            logging.info("parameters = %s", parameters)
            processors.append(CallsiteInfoAdder(parameters=parameters))

        logger = logging.Logger(sys._getframe().f_code.co_name)
        string_io = StringIO()
        handler = logging.StreamHandler(string_io)
        formatter = ProcessorFormatter(
            processors=[*processors, JSONRenderer()],
        )
        handler.setFormatter(formatter)
        handler.setLevel(0)
        logger.addHandler(handler)
        logger.setLevel(0)
        callsite_params = self.get_callsite_params()
        logger.info("test message")

        callsite_params = self.filter_parameter_dict(callsite_params, parameter_strings)
        out = json.loads(string_io.getvalue())
        assert isinstance(out, dict)
        out_item_set = set(out.items())
        expected_item_set = set(
            {
                "event": "test message",
                **callsite_params,
            }.items()
        )
        assert expected_item_set.issubset(out_item_set), (
            f"{out_item_set}"
            "\nshould be a superset of\n"
            f"{expected_item_set}"
            "\nbut it is missing\n"
            f"{expected_item_set.difference(out_item_set)}"
        )
        unexpected_params = self.invert_parameter_strings(parameter_strings)
        assert unexpected_params.isdisjoint(out.keys()), (
            f"{unexpected_params}"
            "\nshould be disjoint with\n"
            f"{out.keys()}"
            "\nbut contains\n"
            f"{unexpected_params.intersection(out.keys())}"
        )

    @pytest.mark.parametrize(
        "parameter_strings",
        [
            None,
            *[{parameter} for parameter in parameter_strings],
            set(),
            parameter_strings,
            {"pathname", "filename"},
            {"module", "funcName"},
        ],
    )
    def test_structlog_log(
        self,
        parameter_strings: Optional[Set[str]],
    ) -> None:
        processors = []
        logging.info("parameter_strings = %s", parameter_strings)
        if parameter_strings is None:
            processors.append(CallsiteInfoAdder())
            parameter_strings = self.parameter_strings
        else:
            parameters = self.filter_parameters(parameter_strings)
            logging.info("parameters = %s", parameters)
            processors.append(CallsiteInfoAdder(parameters=parameters))

        logger = logging.Logger(sys._getframe().f_code.co_name)
        string_io = StringIO()
        handler = logging.StreamHandler(string_io)
        formatter = ProcessorFormatter(
            processors=[JSONRenderer()],
        )
        handler.setFormatter(formatter)
        handler.setLevel(0)
        logger.addHandler(handler)
        logger.setLevel(0)
        ctx: Dict[str, Any] = {}
        bound_logger = BoundLogger(
            logger, [*processors, ProcessorFormatter.wrap_for_formatter], ctx
        )
        callsite_params = self.get_callsite_params()
        bound_logger.info("test message")

        callsite_params = self.filter_parameter_dict(callsite_params, parameter_strings)
        out = json.loads(string_io.getvalue())
        assert isinstance(out, dict)
        out_item_set = set(out.items())
        expected_item_set = set(
            {
                "event": "test message",
                **callsite_params,
            }.items()
        )
        assert expected_item_set.issubset(out_item_set), (
            f"{out_item_set}"
            "\nshould be a superset of\n"
            f"{expected_item_set}"
            "\nbut it is missing\n"
            f"{expected_item_set.difference(out_item_set)}"
        )
        unexpected_params = self.invert_parameter_strings(parameter_strings)
        assert unexpected_params.isdisjoint(out.keys()), (
            f"{unexpected_params}"
            "\nshould be disjoint with\n"
            f"{out.keys()}"
            "\nbut contains\n"
            f"{unexpected_params.intersection(out.keys())}"
        )

    @classmethod
    def invert_parameter_strings(cls, parameter_strings: Set[str]) -> Set[str]:
        return cls.parameter_strings.difference(parameter_strings)

    @classmethod
    def filter_parameters(cls, parameter_strings: Set[str]) -> Set[CallsiteParameter]:
        return {
            parameter
            for parameter in callsite_parameters
            if parameter.value in parameter_strings
        }

    @classmethod
    def filter_parameter_dict(
        cls, input: Dict[str, Any], parameter_strings: Set[str]
    ) -> Dict[str, Any]:
        return {key: value for key, value in input.items() if key in parameter_strings}

    @classmethod
    def get_callsite_params(cls, offset: int = 1) -> Dict[str, Any]:
        """
        This function creates callsite parameters for the line that is `offset` lines after it.
        """
        frame_info = inspect.stack()[1]
        traceback = inspect.getframeinfo(frame_info[0])
        return {
            "pathname": traceback.filename,
            "filename": os.path.basename(traceback.filename),
            "module": os.path.splitext(os.path.basename(traceback.filename))[0],
            "funcName": frame_info.function,
            "lineno": frame_info.lineno + offset,
            "thread": threading.get_ident(),
            "threadName": threading.current_thread().name,
            "process": os.getpid(),
            "processName": get_processname(),
        }
