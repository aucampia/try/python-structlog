import logging
import os
import sys
from typing import Callable, TypeVar

import structlog

from pythonjsonlogger import jsonlogger  # type: ignore[import]
import pytest

GenericT = TypeVar("GenericT")


class ExtraLogFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord) -> str:
        dummy = logging.LogRecord("name", 0, "pathname", 0, "msg", tuple(), None)
        extra_obj = {}
        for key, value in record.__dict__.items():
            if key not in dummy.__dict__:
                extra_obj[key] = value
        record.__dict__["extra_obj"] = extra_obj
        return super().format(record)


def setup_stdlib() -> None:
    handler = logging.StreamHandler(sys.stderr)
    formatter = ExtraLogFormatter(
        datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=(
            "%(asctime)s %(process)d %(thread)x %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s %(extra_obj)s"
        ),
    )
    handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(handler)


def setup_jsonlogger() -> None:
    log_handler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter(
        datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=(
            "%(asctime)s %(msecs)03d %(process)d %(thread)x %(levelname)-8s "
            "%(name)-12s %(module)s %(lineno)s %(funcName)s %(message)s"
        ),
    )
    log_handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(log_handler)


def setup_structlog_stdlib() -> None:
    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.processors.JSONRenderer(),
        ],
        wrapper_class=structlog.stdlib.BoundLogger,
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )


def genlog_stdlib() -> None:
    """
    just a function that logs stuff using stdlib logger.
    """
    logger = logging.getLogger(__name__)
    logger.info(
        "entry", extra={"logger.getEffectiveLevel()": logger.getEffectiveLevel()}
    )


@pytest.mark.parametrize(
    "setup, genlog",
    [
        (setup_stdlib, genlog_stdlib),
        (setup_jsonlogger, genlog_stdlib),
        (setup_structlog_stdlib, genlog_stdlib),
    ],
)
def test_combinations(setup: Callable[[], None], genlog: Callable[[], None]) -> None:
    setup()
    genlog()
