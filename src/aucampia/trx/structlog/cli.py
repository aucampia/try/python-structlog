#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import enum
import json
import logging
import os
import sys
from dataclasses import dataclass
from typing import Callable, List, Optional, TypeVar

import structlog
import structlog._frames
import typer
from pythonjsonlogger import jsonlogger  # type: ignore[import]
from structlog.types import FilteringBoundLogger, Processor

from ._version import __version__
from .extras import CallsiteInfoAdder, CallsiteParameter, ExtraLogFormatter, add_extra

GenericT = TypeVar("GenericT")


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""


@dataclass
class CliContext:
    verbosity: Optional[int]


cli = typer.Typer()


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    ctx.obj = CliContext(verbosity)


@cli.command("version")
def cli_version(ctx: typer.Context) -> None:
    sys.stderr.write(f"{__version__}\n")


class Setup(str, enum.Enum):
    STDLIB = "stdlib"
    JSON_FORMATTER = "json_formatter"
    STRUCTLOG_STDLIB = "structlog_stdlib"
    STRUCTLOG_JSON_FORMATTER = "structlog_json_formatter"
    STRUCTLOG_FORMATTER = "structlog_formatter"


class Genlog(str, enum.Enum):
    STDLIB_EXTRA = "stdlib_extra"
    STDLIB_ARGS = "stdlib_args"
    STRUCTLOG = "structlog"


@cli.command("run")
def cli_run(
    ctx: typer.Context,
    setups: List[Setup] = typer.Option(..., "--setup", "-s"),
    genlogs: List[Genlog] = typer.Option(..., "--genlog", "-g"),
) -> None:
    sys.stderr.write("{}\n".format(json.dumps({"setup": setups, "genlog": genlogs})))

    setup_func: Callable[[], None]
    genlog_func: Callable[[], None]

    for setup in setups:
        setup_func = globals()[f"setup_{setup}"]
        setup_func()
    for genlog in genlogs:
        genlog_func = globals()[f"genlog_{genlog}"]
        genlog_func()


def setup_stdlib() -> None:
    handler = logging.StreamHandler(sys.stderr)
    formatter = ExtraLogFormatter(
        datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=(
            "%(asctime)s %(process)d %(thread)x %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s %(extra)s"
        ),
    )
    handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(handler)


def setup_json_formatter() -> None:
    log_handler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter(
        datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=(
            "%(asctime)s %(msecs)03d %(process)d %(thread)x %(levelname)-8s "
            "%(name)-12s %(module)s %(lineno)s %(funcName)s %(message)s"
        ),
    )
    log_handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(log_handler)


def setup_structlog_stdlib() -> None:
    logging.basicConfig(
        format="%(message)s",
        stream=sys.stderr,
        level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    )
    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.processors.JSONRenderer(),
        ],
        wrapper_class=structlog.stdlib.BoundLogger,
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )


def setup_structlog_json_formatter() -> None:
    log_handler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter(
        datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=(
            "%(asctime)s %(msecs)03d %(process)d %(thread)x %(levelname)-8s "
            "%(name)-12s %(module)s %(lineno)s %(funcName)s %(message)s"
        ),
    )
    log_handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(log_handler)
    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.stdlib.render_to_log_kwargs,
        ],
        wrapper_class=structlog.stdlib.BoundLogger,
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )


def setup_structlog_formatter() -> None:
    shared_processors: List[Processor] = []
    structlog.configure(
        processors=shared_processors
        + [
            structlog.stdlib.filter_by_level,
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )

    formatter = structlog.stdlib.ProcessorFormatter(
        foreign_pre_chain=[add_extra, *shared_processors],
        keep_stack_info=True,
        processors=[
            CallsiteInfoAdder(
                {
                    CallsiteParameter.FUNC_NAME,
                    CallsiteParameter.FILENAME,
                    CallsiteParameter.LINENO,
                    CallsiteParameter.THREAD,
                }
            ),
            structlog.stdlib.add_log_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.stdlib.ProcessorFormatter.remove_processors_meta,
            structlog.processors.JSONRenderer(),
        ],
    )
    log_handler = logging.StreamHandler(stream=sys.stderr)
    log_handler.setFormatter(formatter)
    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    root_logger.addHandler(log_handler)


def genlog_stdlib_extra() -> None:
    logger = logging.getLogger(__name__ + ".genlog_stdlib_extra")
    logger.info("entry", extra={"foo": "bar", "what": "stdlib_extra"})
    logger.info("fmt %s", "is a thing")
    try:
        raise RuntimeError("is here ...")
    except Exception:
        logger.info(
            "caught", exc_info=True, extra={"foo": "bar", "what": "stdlib_extra"}
        )


def genlog_structlog() -> None:
    logger: FilteringBoundLogger = structlog.getLogger(__name__ + ".genlog_structlog")
    logger.info("entry", foo="bar", what="stdlib")
    logger.info("fmt is a thing")
    try:
        raise RuntimeError("is here ...")
    except Exception:
        logger.info("caught", exc_info=True, foo="bar", what="stdlib")
    logger.bind()


def main() -> None:
    cli()


if __name__ == "__main__":
    main()
