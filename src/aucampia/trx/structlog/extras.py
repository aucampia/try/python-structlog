import enum
import inspect
import logging
import os
import sys
import threading
from typing import Any, Callable, Dict, List, Optional, Set, Tuple

import structlog
import structlog._frames
from structlog.types import EventDict


class CallsiteParameter(str, enum.Enum):
    PATHNAME = "pathname"
    FILENAME = "filename"
    MODULE = "module"
    FUNC_NAME = "funcName"
    LINENO = "lineno"
    THREAD = "thread"
    THREAD_NAME = "threadName"
    PROCESS = "process"
    PROCESS_NAME = "processName"


callsite_parameters: Set[CallsiteParameter] = set(CallsiteParameter)


def get_processname() -> str:
    processName = "MainProcess"
    mp: Any = sys.modules.get("multiprocessing")
    if mp is not None:
        # Errors may occur if multiprocessing has not finished loading
        # yet - e.g. if a custom import hook causes third-party code
        # to run when multiprocessing calls import. See issue 8200
        # for an example
        try:
            processName = mp.current_process().name
        except Exception:
            pass
    return processName


class CallsiteInfoAdder:
    __slots__ = ["_parameters", "_active_handlers"]

    _handlers: Dict[CallsiteParameter, Callable[[str, inspect.Traceback], Any]] = {
        CallsiteParameter.PATHNAME: lambda module, frame_info: frame_info.filename,
        CallsiteParameter.FILENAME: lambda module, frame_info: os.path.basename(
            frame_info.filename
        ),
        CallsiteParameter.MODULE: lambda module, frame_info: os.path.splitext(
            os.path.basename(frame_info.filename)
        )[0],
        CallsiteParameter.FUNC_NAME: lambda module, frame_info: frame_info.function,
        CallsiteParameter.LINENO: lambda module, frame_info: frame_info.lineno,
        CallsiteParameter.THREAD: lambda module, frame_info: threading.get_ident(),
        CallsiteParameter.THREAD_NAME: lambda module, frame_info: threading.current_thread().name,
        CallsiteParameter.PROCESS: lambda module, frame_info: os.getpid(),
        CallsiteParameter.PROCESS_NAME: lambda module, frame_info: get_processname(),
    }

    def __init__(
        self, parameters: Set[CallsiteParameter] = callsite_parameters
    ) -> None:
        self._parameters = parameters
        self._active_handlers: List[
            Tuple[CallsiteParameter, Callable[[str, inspect.Traceback], Any]]
        ] = []
        for parameter in self._parameters:
            self._active_handlers.append((parameter, self._handlers[parameter]))

    def __call__(
        self, logger: logging.Logger, name: str, event_dict: EventDict
    ) -> EventDict:
        record: Optional[logging.LogRecord] = event_dict.get("_record")
        if record is not None:
            for parameter in self._parameters:
                event_dict[parameter.value] = record.__dict__[parameter.value]
        else:
            frame, module = structlog._frames._find_first_app_frame_and_name(
                additional_ignores=[__name__]
            )
            frame_info = inspect.getframeinfo(frame)
            for parameter, handler in self._active_handlers:
                handler(module, frame_info)
                event_dict[parameter.value] = handler(module, frame_info)
        return event_dict


def add_callsite_info(
    logger: logging.Logger, method_name: str, event_dict: EventDict
) -> EventDict:
    record: Optional[logging.LogRecord] = event_dict.get("_record")
    if record is not None:
        event_dict["funcName"] = record.funcName
        event_dict["thread"] = record.thread
        event_dict["threadName"] = record.threadName
        event_dict["pathname"] = record.pathname
        event_dict["filename"] = record.filename
        event_dict["lineno"] = record.lineno
        event_dict["module"] = record.module
        event_dict["process"] = record.process
        event_dict["processName"] = record.processName
    else:
        frame, module = structlog._frames._find_first_app_frame_and_name(
            additional_ignores=[__name__]
        )
        frame_info = inspect.getframeinfo(frame)
        event_dict["funcName"] = frame_info.function
        event_dict["thread"] = threading.get_ident()
        event_dict["threadName"] = threading.current_thread().name
        event_dict["pathname"] = frame_info.filename
        event_dict["filename"] = os.path.basename(frame_info.filename)
        event_dict["lineno"] = frame_info.lineno
        event_dict["module"] = module
        event_dict["process"] = os.getpid()
        processName = "MainProcess"
        mp: Any = sys.modules.get("multiprocessing")
        if mp is not None:
            # Errors may occur if multiprocessing has not finished loading
            # yet - e.g. if a custom import hook causes third-party code
            # to run when multiprocessing calls import. See issue 8200
            # for an example
            try:
                processName = mp.current_process().name
            except Exception:
                pass

        event_dict["processName"] = processName

    return event_dict


_BLANK_LOGRECORD = logging.LogRecord("name", 0, "pathname", 0, "msg", tuple(), None)


def get_extra(record: logging.LogRecord) -> Dict[str, str]:
    extra: Dict[str, str] = {}
    for key, value in record.__dict__.items():
        if key not in _BLANK_LOGRECORD.__dict__:
            extra[key] = value
    return extra


class ExtraLogFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord) -> str:
        record.__dict__["extra"] = get_extra(record)
        return super().format(record)


def add_extra(
    logger: logging.Logger, method_name: str, event_dict: EventDict
) -> EventDict:
    """
    Add extra `logging.LogRecord` keys to the event dictionary.

    This function is useful for adding data passed in the ``extra`` parameter
    of the `logging` module's log methods to the event dictionary.

    .. versionadded:: 21.5.0
    """
    record: Optional[logging.LogRecord] = event_dict.get("_record")
    if record is not None:
        for key, value in record.__dict__.items():
            if key not in _BLANK_LOGRECORD.__dict__:
                event_dict[key] = value
    return event_dict
